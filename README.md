# PlantUML GTK

PlantUML GTK is simplistic PlantUML editor for GNOME.

![Screenshot of application](screenshots/app.png "Screenshot of application")

# License

PlantUML GTK is released under GNU General Public License (GPL) version 3 or
later, see the [COPYING](COPYING) for more information.
