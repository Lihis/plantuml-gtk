/* app.h
 *
 * Copyright 2020-2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtkmm/application.h>

class MainWindow;

class App : public Gtk::Application
{
protected:
  App();

public:
  /**
   * @brief Create application
   *
   * @return Glib::RefPtr<App>
   */
  static Glib::RefPtr<App> create();

protected:
  /**
   * @brief Create window and show it
   *
   * @return
   */
  void on_activate() override;

  /**
   * @brief Handle opening files
   *
   * @param files
   * @param hint
   * @return
   */
  void on_open(const Gio::Application::type_vec_files& files, const Glib::ustring& hint) override;

private:
  /**
   * @brief Create application window
   *
   * @return MainWindow*
   */
  MainWindow* create_appwindow();

  /**
   * @brief Destroy window when it's hidden
   *
   * @param window
   * @return
   */
  static void on_hide_window(Gtk::Window* window);

  /**
   * @brief Action quit handler
   *
   * @return
   */
  void on_action_quit();

  /**
   * @brief Action about handler
   *
   * @return
   */
  void on_action_about();

  /**
   * @brief Action preferences handler
   *
   * @return
   */
  void on_action_preferences();
};

