/* editor.cpp
 *
 * Copyright 2020-2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "editor.h"
#include "filechooser.h"
#include <glibmm/i18n.h>
#include <gtksourceviewmm/init.h>
#include <utility>

Editor::Editor(BaseObjectType* object, Glib::RefPtr<Gtk::Builder> builder)
: Gtk::HPaned(object), m_builder(std::move(builder)), m_markdown(nullptr), m_viewer(nullptr)
{
  Gsv::init();

  m_builder->get_widget_derived("editor", m_markdown);
  if (!m_markdown)
    throw std::runtime_error("No \"editor\" in object editor.ui");

  m_markdown->signal_stopped_typing().connect(sigc::mem_fun(*this, &Editor::on_stopped_typing));
  m_markdown->signal_dirtiness_changed().connect(
    sigc::mem_fun(*this, &Editor::on_dirtiness_changed));

  m_builder->get_widget_derived("viewer", m_viewer);
  if (!m_viewer)
    throw std::runtime_error("No \"viewer\" in object editor.ui");
}

Editor::Editor(BaseObjectType* object,
  const Glib::RefPtr<Gtk::Builder>& builder,
  const Glib::ustring& name)
: Editor(object, builder)
{
  m_filename = name;
}

Editor::Editor(BaseObjectType* object,
  const Glib::RefPtr<Gtk::Builder>& builder,
  Glib::RefPtr<Gio::File> file)
: Editor(object, builder)
{
  char* contents = nullptr;
  gsize length = 0;

  m_file = std::move(file);
  m_file->load_contents(contents, length);
  m_markdown->get_source_buffer()->begin_not_undoable_action();
  m_markdown->get_source_buffer()->set_text(contents, (contents + length));
  m_markdown->get_source_buffer()->end_not_undoable_action();
  m_markdown->get_source_buffer()->set_modified(false);
}

Editor::type_signal_title_changed
Editor::signal_title_changed()
{
  return m_signal_title_changed;
}

void
Editor::save(Gtk::Window& parent)
{
  if (!m_file)
  {
    FileChooser dialog(parent);

    if (dialog.save_plantuml(get_filename()) == Gtk::RESPONSE_ACCEPT)
    {
      auto filename = dialog.get_current_name();

      if (dialog.get_filter()->get_name() == _("PlantUML files"))
        if (filename.find_last_of(".pu") == std::string::npos)
          dialog.set_current_name(filename.append(".pu"));

      m_file = Gio::File::create_for_path(dialog.get_filename());
      m_signal_title_changed.emit(get_filename(false));
      auto stream = m_file->create_file();
      stream->write(m_markdown->get_buffer()->get_text());
      m_markdown->get_source_buffer()->set_modified(false);
    }
  }
  else
  {
    std::string new_etag;
    m_file->replace_contents(m_markdown->get_buffer()->get_text(), "", new_etag, true);
    m_signal_title_changed.emit(get_filename(false));
    m_markdown->get_source_buffer()->set_modified(false);
  }
}

void
Editor::export_diagram(Gtk::Window& parent)
{
  FileChooser dialog(parent);

  if (m_file)
    dialog.set_current_folder(m_file->get_parent()->get_path());

  if (dialog.save_diagram(get_filename(false) + ".png") == Gtk::RESPONSE_ACCEPT)
  {
    auto filename = dialog.get_current_name();

    if (dialog.get_filter()->get_name() == _("PNG"))
      if (filename.find_last_of(".png") == std::string::npos)
        dialog.set_current_name(filename.append(".pu"));

    auto pixbuf = m_viewer->get_pixbuf();
    pixbuf->save(dialog.get_filename(), "png", {}, {});
  }
}

bool
Editor::has_unsaved_changes() const
{
  return m_markdown->get_source_buffer()->get_modified();
}

bool
Editor::get_save_as() const
{
  return !m_file;
}

Glib::ustring
Editor::get_filename(bool with_extension) const
{
  Glib::ustring name;

  if (m_file)
    name = m_file->get_basename();
  else
    name = m_filename;

  auto found = name.find_last_of(".pu");
  if (with_extension && found == std::string::npos)
    name.append(".pu");
  else if (!with_extension && found != std::string::npos)
    name = name.substr(0, found - 2);

  return name;
}

void
Editor::on_stopped_typing()
{
  m_viewer->update(m_markdown->get_buffer()->get_text());
}

void
Editor::on_dirtiness_changed(bool dirty)
{
  Glib::ustring title(dirty ? "*" : "");

  title.append(get_filename(false));

  m_signal_title_changed.emit(title);
}

