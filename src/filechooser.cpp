/* filechooser.cpp
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "filechooser.h"
#include <glibmm/i18n.h>

FileChooser::FileChooser(Gtk::Window& parent)
: Gtk::FileChooserDialog(parent, _("Please choose a file"))
{
  add_button("_Cancel", Gtk::RESPONSE_CANCEL);
}

Gtk::ResponseType
FileChooser::open()
{
  add_plantuml_filter();

  add_button("_Open", Gtk::RESPONSE_ACCEPT);
  set_action(Gtk::FILE_CHOOSER_ACTION_OPEN);

  return static_cast<Gtk::ResponseType>(run());
}

Gtk::ResponseType
FileChooser::save_plantuml(const Glib::ustring& name)
{
  add_button("_Save", Gtk::RESPONSE_ACCEPT);
  set_action(Gtk::FILE_CHOOSER_ACTION_SAVE);

  set_current_name(name);
  add_plantuml_filter();

  return static_cast<Gtk::ResponseType>(run());
}

Gtk::ResponseType
FileChooser::save_diagram(const Glib::ustring& name)
{
  add_button("_Save", Gtk::RESPONSE_ACCEPT);
  set_action(Gtk::FILE_CHOOSER_ACTION_SAVE);

  set_current_name(name);

  auto filter_png = Gtk::FileFilter::create();
  filter_png->set_name(_("PNG"));
  filter_png->add_pattern("*.png");
  add_filter(filter_png);

  return static_cast<Gtk::ResponseType>(run());
}

void
FileChooser::add_plantuml_filter()
{
  auto filter_pu = Gtk::FileFilter::create();
  filter_pu->set_name(_("PlantUML files"));
  filter_pu->add_pattern("*.pu");
  filter_pu->add_pattern("*.puml");
  add_filter(filter_pu);

  auto filter_any = Gtk::FileFilter::create();
  filter_any->set_name(_("All Files"));
  filter_any->add_pattern("*");
  add_filter(filter_any);
}
