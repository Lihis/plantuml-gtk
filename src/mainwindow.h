/* mainwindow.h
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtkmm/applicationwindow.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/menubutton.h>
#include <gtkmm/stack.h>
#include <gtkmm/stackswitcher.h>

class MainWindow : public Gtk::ApplicationWindow
{
public:
  MainWindow(BaseObjectType* object, Glib::RefPtr<Gtk::Builder> builder);

  /**
   * @brief Create MainWindow
   *
   * @return MainWindow*
   */
  static MainWindow* create();

  /**
   * @brief Handle file opening
   *
   * @param file
   * @return
   */
  void open_file_view(const Glib::RefPtr<Gio::File>& file);

protected:
  /**
   * @brief On show and stack is empty call @ref add_editor and call base
   *
   * @return
   */
  void on_show() override;

  /**
   * @brief Go trough @ref m_stack and check if any has unsaved changes
   *
   * Ask confirmation for any @ref m_stack editor which has unsaved changes
   * and if user answers Gtk::RESPONSE_CANCEL do not propagate the event further
   *
   * @param event
   * @return bool
   */
  bool on_delete_event(GdkEventAny* event) override;

private:
  /**
   * @brief Add new editor to the @ref m_stack
   *
   * @return
   */
  void add_editor();

  /**
   * @brief On @ref m_open click show file chooser
   *
   * @return
   */
  void on_open_clicked();

  /**
   * @brief Save button clicked callback
   *
   * @return
   */
  void on_save_clicked();

  /**
   * @brief Export button clicked callback
   *
   * @return
   */
  void on_export_clicked();

  /**
   * @brief Signal when a new tab is added to @ref m_tabs
   *
   * Registers signal handler for signal_button_press_event on @p widget
   *
   * @param widget - Tab button
   * @return
   */
  void on_tab_add(Gtk::Widget* widget);

  /**
   * @brief Signal handler for signals registered in @ref on_tab_add
   *
   * Handles left and right clicks. Right click shows popover menu.
   *
   * @param event_button
   * @param widget - @ref m_tabs tab button which was clicked
   * @return bool
   */
  bool on_tab_clicked(GdkEventButton* event_button, Gtk::Widget* widget);

  /**
   * @brief Close requested for the tab
   *
   * @return
   */
  void on_tab_close();

  /**
   * @brief Signal from @ref Editor when title should be updated
   *
   * @param title
   * @return
   */
  void on_title_changed(const Glib::ustring& title);

  Glib::RefPtr<Gtk::Builder> m_builder;
  Gtk::Button* m_open;
  Gtk::Button* m_new_tab;
  Gtk::StackSwitcher* m_tabs;
  Gtk::Button* m_save;
  Gtk::Button* m_export;
  Gtk::MenuButton* m_menu;
  Gtk::Popover* m_popover;
  Gtk::Stack* m_stack;
  size_t m_untitled_count;
};
