/* plantuml.h
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gdkmm/pixbufloader.h>
#include <glibmm/dispatcher.h>
#include <glibmm/ustring.h>
#include <mutex>
#include <thread>
#include <vector>

class Viewer;

class PlantUML
{
public:
  explicit PlantUML(Viewer* viewer);
  ~PlantUML();

protected:
  /**
   * @brief Get generated diagram
   *
   * @return Glib::RefPtr<Gdk::Pixbuf>
   */
  Glib::RefPtr<Gdk::Pixbuf> get_diagram();

  /**
   * @brief Queue diagram to be generated
   *
   * @param text
   * @return
   */
  void generate(const Glib::ustring& text);

private:
  /**
   * @brief Call @ref plantuml_generate
   *
   * @return
   */
  void start_generation();

  /**
   * @brief Call @ref plantuml_generate when existing generation finishes
   *
   * @return bool
   */
  bool on_timer_generate();

  /**
   * @brief Generate the diagram
   *
   * @return
   */
  void plantuml_generate();

  Viewer* m_viewer;
  Glib::RefPtr<Gdk::PixbufLoader> m_loader;
  bool m_generating;
  sigc::connection m_timer;
  Glib::ustring m_diagram;
  mutable std::mutex m_mutex;
  std::thread* m_thread;
};
