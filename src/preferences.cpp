/* preferences.cpp
 *
 * Copyright 2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "preferences.h"
#include <glibmm/i18n.h>

#define PREFERENCES_UI "preferences.ui"

#define BUILDER_GET_WIDGET(BUILDER, NAME) \
  BUILDER->get_widget(#NAME, m_##NAME);   \
  if (m_##NAME == nullptr)                \
  throw std::runtime_error("No \"" #NAME "\" in object " PREFERENCES_UI)

Preferences::Preferences(BaseObjectType* object, Glib::RefPtr<Gtk::Builder> builder)
: Gtk::Window(object)
, m_builder(std::move(builder))
, m_editor_use_tab(nullptr)
, m_editor_indent_width(nullptr)
, m_viewer_timeout_ms(nullptr)
{
  BUILDER_GET_WIDGET(m_builder, editor_use_tab);
  BUILDER_GET_WIDGET(m_builder, editor_indent_width);
  BUILDER_GET_WIDGET(m_builder, viewer_timeout_ms);

  set_title(_("Preferences"));

  m_settings = Gio::Settings::create("net.lihis.plantuml-gtk");
  m_settings->bind("editor-use-tabs", m_editor_use_tab->property_active());
  m_settings->bind("editor-indent-width", m_editor_indent_width->property_value());
  m_settings->bind("viewer-preview-timeout", m_viewer_timeout_ms->property_value());
}

Preferences*
Preferences::create(Gtk::Window& parent)
{
  auto builder = Gtk::Builder::create_from_resource("/net/lihis/plantuml-gtk/" PREFERENCES_UI);

  Preferences* window = nullptr;
  builder->get_widget_derived("window", window);
  builder->get_widget("window", window);
  if (window == nullptr)
    throw std::runtime_error("No \"window\" in object " PREFERENCES_UI);

  window->set_transient_for(parent);
  window->set_modal(true);

  return window;
}

