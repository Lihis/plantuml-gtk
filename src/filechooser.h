/* filechooser.h
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtkmm/filechooserdialog.h>

class FileChooser : public Gtk::FileChooserDialog
{
public:
  explicit FileChooser(Gtk::Window& parent);

  /**
   * Open a file
   *
   * @return Gtk::ResponseType
   */
  Gtk::ResponseType open();

  /**
   * Save PlantUML file
   *
   * @param name
   * @return Gtk::ResponseType - Gtk::RESPONSE_ACCEPT on save
   */
  Gtk::ResponseType save_plantuml(const Glib::ustring& name);

  /**
   * Save diagram as PNG
   *
   * @param name
   * @return Gtk::ResponseType - Gtk::RESPONSE_ACCEPT on save
   */
  Gtk::ResponseType save_diagram(const Glib::ustring& name);

private:
  /**
   * Add filter for PlantUML files (.pu, .puml) files and "All files"
   */
  void add_plantuml_filter();
};
