/* mainwindow.cpp
 *
 * Copyright 2020-2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "editor.h"
#include "filechooser.h"
#include "mainwindow.h"
#include <glibmm/i18n.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/radiobutton.h>
#include <utility>

#define MAINWINDOW_UI "mainwindow.ui"

#define BUILDER_GET_WIDGET(BUILDER, NAME, WIDGET) \
  BUILDER->get_widget(#NAME, WIDGET);             \
  if (!WIDGET)                                    \
  throw std::runtime_error("No \"" #NAME "\" in object " MAINWINDOW_UI)

MainWindow::MainWindow(BaseObjectType* object, Glib::RefPtr<Gtk::Builder> builder)
: Gtk::ApplicationWindow(object)
, m_builder(std::move(builder))
, m_open(nullptr)
, m_new_tab(nullptr)
, m_tabs(nullptr)
, m_save(nullptr)
, m_export(nullptr)
, m_menu(nullptr)
, m_popover(nullptr)
, m_stack(nullptr)
, m_untitled_count(0)
{
  BUILDER_GET_WIDGET(m_builder, open, m_open);
  BUILDER_GET_WIDGET(m_builder, new_tab, m_new_tab);
  BUILDER_GET_WIDGET(m_builder, tabs, m_tabs);
  BUILDER_GET_WIDGET(m_builder, save, m_save);
  BUILDER_GET_WIDGET(m_builder, export, m_export);
  BUILDER_GET_WIDGET(m_builder, menu, m_menu);
  BUILDER_GET_WIDGET(m_builder, popover, m_popover);
  BUILDER_GET_WIDGET(m_builder, stack, m_stack);

  m_new_tab->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::add_editor));

  {
    Glib::RefPtr<Gio::Menu> menu = Gio::Menu::create();
    menu->append(_("Preferences"), "app.preferences");
    menu->append(_("About"), "app.about");

    m_menu->set_menu_model(menu);
  }
  add_action("export", sigc::mem_fun(*this, &MainWindow::on_export_clicked));
  add_action("tab_new", sigc::mem_fun(*this, &MainWindow::add_editor));

  {
    Glib::RefPtr<Gio::Menu> menu = Gio::Menu::create();
    menu->append(_("Close"), "win.tab_close");
    m_popover->bind_model(menu);
  }
  add_action("tab_close", sigc::mem_fun(*this, &MainWindow::on_tab_close));

  m_open->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_open_clicked));
  m_save->signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_save_clicked));

  m_tabs->signal_add().connect(sigc::mem_fun(*this, &MainWindow::on_tab_add));

  set_title("PlantUML GTK");
}

MainWindow*
MainWindow::create()
{
  auto builder = Gtk::Builder::create_from_resource("/net/lihis/plantuml-gtk/" MAINWINDOW_UI);

  MainWindow* window = nullptr;
  builder->get_widget_derived("main_window", window);
  BUILDER_GET_WIDGET(builder, main_window, window);

  return window;
}

void
MainWindow::open_file_view(const Glib::RefPtr<Gio::File>& file)
{
  auto editorBuilder = Gtk::Builder::create_from_resource("/net/lihis/plantuml-gtk/editor.ui");
  Editor* editor;
  editorBuilder->get_widget_derived("paned", editor, file);
  if (!editor)
    throw std::runtime_error("No \"paned\" in object editor.ui");

  editor->signal_title_changed().connect(sigc::mem_fun(*this, &MainWindow::on_title_changed));
  m_stack->add(*editor, file->get_path(), file->get_basename());
  m_stack->set_visible_child(*editor);
}

void
MainWindow::on_show()
{
  if (!m_stack->get_visible_child())
    add_editor();

  Gtk::ApplicationWindow::on_show();
}

bool
MainWindow::on_delete_event(GdkEventAny* event)
{
  bool can_exit = true;

  for (auto* page : m_stack->get_children())
  {
    auto editor = reinterpret_cast<Editor*>(page);
    if (editor->has_unsaved_changes())
    {
      m_stack->set_visible_child(*page);

      auto title = Glib::ustring::compose(
        _(R"(Save changes to "%1" before closing?)"), editor->get_filename(false));
      Gtk::MessageDialog dialog(*this, title, false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_NONE);
      dialog.set_secondary_text(_("All unsaved changes will be lost."));
      dialog.set_default_response(Gtk::RESPONSE_YES);

      auto close = dialog.add_button(_("Close without Saving"), Gtk::RESPONSE_NO);
      close->get_style_context()->add_class(GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);

      dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
      dialog.add_button(editor->get_save_as() ? _("Save As…") : _("Save"), Gtk::RESPONSE_YES);

      switch (dialog.run())
      {
      case Gtk::RESPONSE_NO:
        break;
      case Gtk::RESPONSE_CANCEL:
        can_exit = false;
        break;
      case Gtk::RESPONSE_YES:
        // FIXME: save() should return an error or Cancel to prevent unintentional closing
        editor->save(*this);
        break;
      default:
        break;
      }
    }
  }

  if (can_exit)
    Gtk::ApplicationWindow::on_delete_event(event);

  return !can_exit;
}

void
MainWindow::add_editor()
{
  ++m_untitled_count;
  Glib::ustring title = Glib::ustring::compose(_("Untitled Diagram %1"), m_untitled_count);

  auto editorBuilder = Gtk::Builder::create_from_resource("/net/lihis/plantuml-gtk/editor.ui");
  Editor* editor;
  editorBuilder->get_widget_derived("paned", editor, title);
  if (!editor)
    throw std::runtime_error("No \"paned\" in object editor.ui");

  editor->signal_title_changed().connect(sigc::mem_fun(*this, &MainWindow::on_title_changed));
  m_stack->add(*editor, title, title);
}

void
MainWindow::on_open_clicked()
{
  FileChooser dialog(*this);
  if (dialog.open() == Gtk::RESPONSE_ACCEPT)
  {
    open_file_view(dialog.get_file());
  }
}

void
MainWindow::on_save_clicked()
{
  auto page = reinterpret_cast<Editor*>(m_stack->get_visible_child());
  if (!page)
    return;

  page->save(*this);
}

void
MainWindow::on_export_clicked()
{
  auto page = reinterpret_cast<Editor*>(m_stack->get_visible_child());
  if (!page)
    return;

  page->export_diagram(*this);
}

void
MainWindow::on_tab_add(Gtk::Widget* widget)
{
  auto* button = dynamic_cast<Gtk::Button*>(widget);
  button->signal_button_press_event().connect(
    sigc::bind(sigc::mem_fun(*this, &MainWindow::on_tab_clicked), button));
}

bool
MainWindow::on_tab_clicked(GdkEventButton* event_button, Gtk::Widget* widget)
{
  constexpr guint mouse_right = 3;

  if (event_button->type == GDK_BUTTON_PRESS && event_button->button == mouse_right)
  {
    dynamic_cast<Gtk::RadioButton*>(widget)->set_active(true);
    m_popover->set_relative_to(*widget);
    m_popover->popup();

    return true;
  }

  return false;
}

void
MainWindow::on_tab_close()
{
  auto visible_child = m_stack->get_visible_child();
  auto editor = reinterpret_cast<Editor*>(visible_child);
  if (!editor)
    return;

  if (editor->has_unsaved_changes())
  {
    auto dialog = Gtk::MessageDialog(*this, "TODO: unsaved changes, not closing..");
    dialog.run();
    return;
  }

  m_stack->remove(*visible_child);
  if (m_stack->get_children().empty())
    add_editor();
}

void
MainWindow::on_title_changed(const Glib::ustring& title)
{
  set_title(title + " - PlantUML GTK");

  auto page = m_stack->get_visible_child();
  if (page)
    m_stack->child_property_title(*page) = title;
}

