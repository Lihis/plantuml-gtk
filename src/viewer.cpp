/* viewer.cpp
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "viewer.h"
#include <utility>

Viewer::Viewer(BaseObjectType* base_object, Glib::RefPtr<Gtk::Builder> builder)
: Gtk::Image(base_object), PlantUML(this), m_builder(std::move(builder))
{
  m_dispatcher.connect(sigc::mem_fun(*this, &Viewer::on_diagram_generated));
}

void
Viewer::update(const Glib::ustring& text)
{
  generate(text);
}

void
Viewer::notify()
{
  m_dispatcher.emit();
}

void
Viewer::on_size_allocate(Gtk::Allocation& allocation)
{
  scale(get_pixbuf());

  Gtk::Image::on_size_allocate(allocation);
}

void
Viewer::on_diagram_generated()
{
  auto diagram = get_diagram();
  scale(diagram);
  set(diagram);
}

void
Viewer::scale(Glib::RefPtr<Gdk::Pixbuf> pixbuf)
{
  if (!get_pixbuf())
    return;

  int maxWidth = 0;
  int maxHeight = 0;
  get_size_request(maxWidth, maxHeight);
  maxWidth = std::max(maxWidth, get_width());
  maxHeight = std::max(maxHeight, get_height());

  int width = pixbuf->get_width();
  int height = pixbuf->get_height();
  if (width > maxWidth)
  {
    float ratio = static_cast<float>(maxWidth) / width;
    width = width * ratio;
    height = height * ratio;
    pixbuf->scale_simple(maxWidth, height, Gdk::INTERP_BILINEAR);
  }

  if (height > maxHeight)
  {
    float ratio = static_cast<float>(maxHeight) / height;
    width = width * ratio;
    pixbuf->scale_simple(width, maxHeight, Gdk::INTERP_BILINEAR);
  }
}
