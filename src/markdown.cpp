/* markdown.cpp
 *
 * Copyright 2020-2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "markdown.h"
#include <giomm/settings.h>
#include <glibmm/main.h>
#include <gtkmm/cssprovider.h>
#include <utility>

Markdown::Markdown(BaseObjectType* base_object, Glib::RefPtr<Gtk::Builder> builder)
: Gsv::View(base_object), m_builder(std::move(builder))
{
  auto css = Gtk::CssProvider::create();
  css->load_from_data("textview { font-family: Monospace; }");
  get_style_context()->add_provider(css, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  get_source_buffer()->signal_changed().connect(sigc::mem_fun(*this, &Markdown::on_text_changed));

  m_settings = Gio::Settings::create("net.lihis.plantuml-gtk");
  set_insert_spaces_instead_of_tabs(!m_settings->get_boolean("editor-use-tabs"));
  set_indent_width(m_settings->get_int("editor-indent-width"));

  m_settings->signal_changed("editor-use-tabs")
    .connect(sigc::mem_fun(*this, &Markdown::on_settings_changed));
  m_settings->signal_changed("editor-indent-width")
    .connect(sigc::mem_fun(*this, &Markdown::on_settings_changed));
}

Markdown::type_signal_stopped_typing
Markdown::signal_stopped_typing()
{
  return m_signal_stopped_typing;
}

Markdown::type_signal_dirtiness_changed
Markdown::signal_dirtiness_changed()
{
  return m_signal_dirtiness_changed;
}

void
Markdown::on_undo()
{
  start_timer();

  Gsv::View::on_undo();
}

void
Markdown::on_redo()
{
  start_timer();

  Gsv::View::on_redo();
}

void
Markdown::on_text_changed()
{
  start_timer();
}

void
Markdown::start_timer()
{
  m_timestamp = std::chrono::system_clock::now();

  bool dirty = get_source_buffer()->get_modified();
  m_signal_dirtiness_changed.emit(dirty);

  if (!m_timer.connected())
  {
    m_timer = Glib::signal_timeout().connect(sigc::mem_fun(*this, &Markdown::is_typing), 100);
  }
}

bool
Markdown::is_typing()
{
  auto now = std::chrono::system_clock::now();
  auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_timestamp);
  if (diff.count() >= m_settings->get_uint("viewer-preview-timeout"))
  {
    m_signal_stopped_typing.emit();
    return false;
  }

  return true;
}

void
Markdown::on_settings_changed(const Glib::ustring& key)
{
  if (key == "editor-use-tabs")
    set_insert_spaces_instead_of_tabs(!m_settings->get_boolean("editor-use-tabs"));
  else if (key == "editor-indent-widt")
    set_indent_width(m_settings->get_int("editor-indent-width"));
}

