/* markdown.h
 *
 * Copyright 2020-2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define GTK_SOURCE_H_INSIDE 1
#define GTK_SOURCE_COMPILATION 1
#include <gtksourceviewmm/view.h>
#undef GTK_SOURCE_H_INSIDE
#undef GTK_SOURCE_COMPILATION
#include <chrono>
#include <giomm/settings.h>
#include <gtkmm/builder.h>

class Markdown : public Gsv::View
{
public:
  Markdown(BaseObjectType* base_object, Glib::RefPtr<Gtk::Builder> builder);

  typedef sigc::signal<void> type_signal_stopped_typing;
  /**
   * @brief The @ref signal_stopped_typing is emitted when user stops typing
   *
   * Slot prototype:
   * @code{.cpp}
   * void on_stopped_typing()
   * @endcode
   *
   * @return type_signal_stopped_typing
   */
  type_signal_stopped_typing signal_stopped_typing();

  typedef sigc::signal<void, bool> type_signal_dirtiness_changed;
  /**
   * @brief The @ref signal_title_changed is emitted when file name or it's
   * dirtiness changes
   *
   * Slot prototype:
   * @code{.cpp}
   * void on_title_changed(bool)
   * @endcode
   *
   * @return type_signal_dirtiness_changed
   */
  type_signal_dirtiness_changed signal_dirtiness_changed();

protected:
  /**
   * @brief See @ref start_timer. This also calls base class.
   *
   * @return
   */
  void on_undo() override;

  /**
   * @brief See @ref start_timer
   *
   * @return
   */
  void on_redo() override;

  Glib::RefPtr<Gtk::Builder> m_builder;

private:
  /**
   * @brief See @ref start_timer.
   *
   * @return
   */
  void on_text_changed();

  /**
   * @brief Start timer which calls @ref on_update_image when it runs out
   *
   * @return
   */
  void start_timer();

  /**
   * @brief Emit @ref signal_stopped_typing when no interaction for defined time
   *
   * @return bool
   */
  bool is_typing();

  /**
   * @brief Callback for @ref m_settings signal_changed()
   *
   * @return
   */
  void on_settings_changed(const Glib::ustring& key);

  type_signal_stopped_typing m_signal_stopped_typing;
  type_signal_dirtiness_changed m_signal_dirtiness_changed;
  sigc::connection m_timer;
  std::chrono::time_point<std::chrono::system_clock> m_timestamp;

  Glib::RefPtr<Gio::Settings> m_settings;
};

