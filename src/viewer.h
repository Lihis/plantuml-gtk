/* viewer.h
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "plantuml.h"
#include <gdkmm/pixbuf.h>
#include <gtkmm/builder.h>
#include <gtkmm/image.h>

class Viewer
: public Gtk::Image
, private PlantUML
{
public:
  Viewer(BaseObjectType* base_object, Glib::RefPtr<Gtk::Builder> builder);

  /**
   * @brief Set image from data
   *
   * @param text
   * @param offset
   * @return
   */
  void update(const Glib::ustring& text);

  /**
   * @brief Called from @ref PlantUML when image is generated
   *
   * @return
   */
  void notify();

protected:
  /**
   * @brief Scale image when allocated size is changed and call base method
   *
   * @param allocation
   * @return
   */
  void on_size_allocate(Gtk::Allocation& allocation) override;

  Glib::RefPtr<Gtk::Builder> m_builder;

private:
  /**
   * @brief Update Gtk::Image from generated diagram
   *
   * @param diagram
   * @return
   */
  void on_diagram_generated();

  /**
   * @brief Scale @p pixbuf image to fit into allocated are for the Gtk::Image
   *
   * @param pixbuf
   * @return
   */
  void scale(Glib::RefPtr<Gdk::Pixbuf> pixbuf);

  Glib::Dispatcher m_dispatcher;
};
