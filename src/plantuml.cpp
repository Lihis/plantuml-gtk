/* plantuml.cpp
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "plantuml.h"
#include "viewer.h"
#include <gdkmm/pixbufloader.h>
#include <sys/wait.h>

PlantUML::PlantUML(Viewer* viewer) : m_viewer(viewer), m_generating(false), m_thread(nullptr)
{
  m_loader = Gdk::PixbufLoader::create();
  try
  {
    m_loader->close();
  }
  catch (...)
  {
  }
}

PlantUML::~PlantUML()
{
  const struct timespec ts = { 0, (100 * 1000 * 1000) };

  while (m_generating)
  {
    ::nanosleep(&ts, nullptr);
  }

  if (m_thread && m_thread->joinable())
    m_thread->join();
  delete m_thread;
}

Glib::RefPtr<Gdk::Pixbuf>
PlantUML::get_diagram()
{
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_loader->get_pixbuf();
}

void
PlantUML::generate(const Glib::ustring& text)
{
  std::lock_guard<std::mutex> lock(m_mutex);
  m_diagram = text;

  if (!m_generating && !m_timer.connected())
    start_generation();
  else if (!m_timer.connected())
    m_timer =
      Glib::signal_timeout().connect(sigc::mem_fun(*this, &PlantUML::on_timer_generate), 100);
}

void
PlantUML::start_generation()
{
  if (m_thread && m_thread->joinable())
  {
    m_thread->join();
    delete m_thread;
  }

  m_generating = true;
  m_thread = new std::thread(&PlantUML::plantuml_generate, this);
}

bool
PlantUML::on_timer_generate()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  if (!m_generating)
  {
    start_generation();
    return false;
  }

  return true;
}

void
PlantUML::plantuml_generate()
{
  int readpipe[2];
  int writepipe[2];
  pid_t pid;

  if (pipe(readpipe) < 0 || pipe(writepipe) < 0)
  {
    perror("pipe");
    return;
  }

  int PARENT_READ = readpipe[0];
  int CHILD_WRITE = readpipe[1];
  int CHILD_READ = writepipe[0];
  int PARENT_WRITE = writepipe[1];

  pid = fork();
  if (pid < 0)
  {
    perror("failed to fork");
  }
  else if (pid == 0)
  {
    ::close(PARENT_WRITE);
    ::close(PARENT_READ);

    dup2(CHILD_READ, 0);
    ::close(CHILD_READ);
    dup2(CHILD_WRITE, 1);
    ::close(CHILD_WRITE);

    execl("/usr/bin/plantuml", "plantuml", "-failfast", "-pipe", nullptr);
    _exit(1);
  }
  else
  {
    ::close(CHILD_READ);
    ::close(CHILD_WRITE);
    m_mutex.lock();
    ::write(PARENT_WRITE, m_diagram.c_str(), m_diagram.length());
    m_mutex.unlock();
    ::close(PARENT_WRITE);

    uint8_t tmp[128];
    memset(tmp, 0, sizeof(tmp));
    auto buffer = static_cast<uint8_t*>(malloc(sizeof(tmp)));
    memset(buffer, 0, sizeof(tmp));
    size_t buf_len = sizeof(tmp);
    size_t offset = 0;

    auto time_now = std::chrono::time_point<std::chrono::system_clock>();
    while (std::chrono::duration_cast<std::chrono::milliseconds>(
             std::chrono::time_point<std::chrono::system_clock>() - time_now)
             .count() < 5000)
    {
      if (waitpid(pid, nullptr, WNOHANG) <= 0)
      {
        break;
      }
    }

    while (true)
    {
      ssize_t len = read(PARENT_READ, tmp, (sizeof(tmp) - 1));
      if (len <= 0)
      {
        break;
      }

      if ((offset + len) > buf_len)
      {
        buf_len += sizeof(tmp);
        buffer = static_cast<uint8_t*>(realloc(buffer, buf_len));
      }

      memcpy(buffer + offset, tmp, len);
      offset += len;
    }

    if (offset > 0)
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_loader = Gdk::PixbufLoader::create();
      m_loader->write(buffer, offset);
      m_loader->close();
    }

    {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_generating = false;
    }

    free(buffer);

    if (offset > 0)
      m_viewer->notify();
  }
}
