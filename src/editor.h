/* editor.h
 *
 * Copyright 2020 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "markdown.h"
#include "viewer.h"
#include <gtkmm/builder.h>
#include <gtkmm/hvpaned.h>

class Editor : public Gtk::HPaned
{
public:
  Editor(BaseObjectType* object, Glib::RefPtr<Gtk::Builder> builder);
  Editor(BaseObjectType* object,
    const Glib::RefPtr<Gtk::Builder>& builder,
    const Glib::ustring& name);
  Editor(BaseObjectType* object,
    const Glib::RefPtr<Gtk::Builder>& builder,
    Glib::RefPtr<Gio::File> file);

  typedef sigc::signal<void, const Glib::ustring&> type_signal_title_changed;
  /**
   * @brief The @ref signal_title_changed is emitted when file name or buffer
   * dirtiness changes
   *
   * Slot prototype:
   * @code{.cpp}
   * void on_title_changed(const Glib::ustring&)
   * @endcode
   *
   * @return
   */
  type_signal_title_changed signal_title_changed();

  /**
   * @brief Save file
   *
   * @param parent
   * @return
   */
  void save(Gtk::Window& parent);

  /**
   * @brief Export the diagram
   *
   * @param parent
   * @return
   */
  void export_diagram(Gtk::Window& parent);

  /**
   * @brief Does the @ref m_markdown have unsaved changes
   *
   * @return bool - True if unsaved changes, false otherwise
   */
  bool has_unsaved_changes() const;

  /**
   * @brief Is File Chooser needed for saving
   *
   * @return bool - True if saving via Save As, false otherwise
   */
  bool get_save_as() const;

  /**
   * @brief Get current file name
   *
   * @return Glib::ustring
   */
  Glib::ustring get_filename(bool with_extension = true) const;

private:
  /**
   * @brief Signal from @ref m_markdown when user stops typing
   *
   * Trigger diagram generation for @ref m_viewer
   *
   * @return
   */
  void on_stopped_typing();

  /**
   * @brief Signal from @ref m_markdown when buffer is dirty
   *
   * @param dirty
   * @return
   */
  void on_dirtiness_changed(bool dirty);

  Glib::RefPtr<Gtk::Builder> m_builder;
  Markdown* m_markdown;
  Viewer* m_viewer;

  type_signal_title_changed m_signal_title_changed;
  Glib::ustring m_filename;
  Glib::RefPtr<Gio::File> m_file;
};
