/* app.cpp
 *
 * Copyright 2020-2022 Tomi Lähteenmäki <lihis@lihis.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app.h"
#include "mainwindow.h"
#include "preferences.h"
#include "version.h"
#include <gtkmm/aboutdialog.h>

App::App() : Gtk::Application("net.lihis.plantuml-gtk", Gio::APPLICATION_HANDLES_OPEN) {}

Glib::RefPtr<App>
App::create()
{
  return Glib::RefPtr<App>(new App());
}

void
App::on_activate()
{
  auto appwindow = create_appwindow();
  appwindow->present();
}

void
App::on_open(const Gio::Application::type_vec_files& files, const Glib::ustring& /* hint */)
{
  MainWindow* appwindow = nullptr;
  auto windows = get_windows();
  if (windows.size() > 0)
    appwindow = dynamic_cast<MainWindow*>(windows[0]);

  if (!appwindow)
    appwindow = create_appwindow();

  for (const auto& file : files)
    appwindow->open_file_view(file);

  appwindow->present();
}

MainWindow*
App::create_appwindow()
{
  auto appwindow = MainWindow::create();

  add_window(*appwindow);

  add_action("quit", sigc::mem_fun(*this, &App::on_action_quit));
  add_action("preferences", sigc::mem_fun(*this, &App::on_action_preferences));
  add_action("about", sigc::mem_fun(*this, &App::on_action_about));

  set_accel_for_action("app.quit", "<Ctrl>Q");
  set_accel_for_action("win.export", "<Ctrl>E");
  set_accel_for_action("win.tab_new", "<Ctrl>N");
  set_accel_for_action("win.tab_close", "<Ctrl>W");

  appwindow->signal_hide().connect(
    sigc::bind<Gtk::Window*>(sigc::ptr_fun(&App::on_hide_window), appwindow));

  return appwindow;
}

void
App::on_hide_window(Gtk::Window* window)
{
  delete window;
}

void
App::on_action_quit()
{
  for (auto window : get_windows())
    window->hide();

  quit();
}

void
App::on_action_about()
{
  Gtk::Window* active_window = get_active_window();
  auto dialog = Gtk::AboutDialog();

  if (active_window)
    dialog.set_transient_for(*active_window);
  dialog.set_program_name("PlantUML GTK");
  dialog.set_version(PACKAGE_VERSION);
  dialog.set_comments("GTK based PlantUML editor");
  dialog.set_copyright("Copyright © 2020 - Tomi Lähteenmäki");
  dialog.set_license_type(Gtk::License::LICENSE_GPL_3_0);
  dialog.set_website("https://gitlab.com/Lihis/plantuml-gtk");

  dialog.run();
}

void
App::on_action_preferences()
{
  auto prefsWindow = Preferences::create(*get_active_window());

  prefsWindow->show();
}

